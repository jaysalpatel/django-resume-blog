from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='blog-home'),
    path('about/', views.about, name='blog-about'),
    path('tech/',  views.tech, name='blog-tech'),
    path('culture/', views.culture, name='blog-culture'),
    path('devops/', views.devops, name='devops blog')
  
]