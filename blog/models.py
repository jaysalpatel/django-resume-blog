from django.db import models
from django.utils import timezone 

from django.contrib.auth.models import User 

##textfield is unrestricted text length amount
# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField()
    date_posted = models.DateTimeField(default=timezone.now)
    ##if a user is deleted, their posts are deleted with models.CASCADE
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    ##dunder, double underscore
    def __str__(self):
        return self.title
    
##class Post1(models.Model):
    

##class News(models.model):
  #  author = models.ManyToManyField()
   # title = models.CharField(max_length=200)
   
#class News(models.Model):
 #   description = models.CharField(max_length=False)
    
