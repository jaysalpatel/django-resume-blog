from django.shortcuts import render
##import post model, .models from models file in the current package/directory 
from .models import Post 
from django.http import HttpResponse



# Create your views here.



def home(request):
    ## return request object, then template html file then context: dictionary with 
    # variable names as the key and their values as the value
    context = {
        ##query all the data in the database created
        'posts': Post.objects.all()
    }
    return render(request, 'blog/home.html', context)
    

def about(request):
    return render(request, 'blog/about.html', {'title': 'About'})

def tech(request):
        
        #'news': News.objects.all()
       # newsapi = NewsApiClient(api_key='')
        #top = newsapi.get_top_headlines(source='techcrunch')
        
        #l = top['articles']
        #description = []
        #news = []
        #img = []
        
        #for i in range(len(1)):
         #   f = l[i]
           # news.append(f('title'))
           #  description.append(f('description'))
            #img.append(f('urlToImage'))
        #list1 = zip(news, description, img)        
        return render(request, 'blog/tech.html', {'tech': 'tech'})

def culture(request):                              ##open dictionary
    return render(request, 'blog/culture.html', {'culture': 'cultures'})

def devops(request):
    return HttpResponse('<h1> Devops page</h1>')


