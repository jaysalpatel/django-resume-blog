from django.shortcuts import render, redirect 
from django.contrib.auth.forms import UserCreationForm

from django.contrib import messages
from .forms import UserRegisterForm

# Create your views here.

def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            ##flash message
            messages.success(request, 'Your profile has been created')
            ##blog-home is the name for url pattern 
            return redirect('blog-home')
    ##get request
    else:
        form = UserRegisterForm()
    ## return request object, then template html file then context: dictionary with 
    # variable names as the key and their values as the value
    # context(instance of the UserCreationForm inside of the variable form
    return render(request, 'users/register.html', {'form': form})


# message.info
# message.success
