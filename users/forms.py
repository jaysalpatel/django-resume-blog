from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm 

class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()
    
    ##this is a nested namespace for configurations and keeps config in one place
    ##
    class Meta:
        ##specify the model we want the form to interact with 
        ##when form validates, its gonna create a new user
        model = User 
        fields = ['username', 'email', 'password1', 'password2']
        
    
    