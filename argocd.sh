kubectl create ns argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

### expose the Argo CD API server
##changing the argocd-server service type to
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'

###port forwarding

kubectl port-forward svc/argocd-server -n argocd 7000:443