#!/bin/bash 
minikube start --cpus 2 --memory 7000

minikube config set driver docker 

minikube addons enable ingress 
minikube dashboard --url