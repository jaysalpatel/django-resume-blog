#!/bin/bash 

kubectl create -f ./prometheus.yaml 

helm install my-prometheus prometheus-community/prometheus --version 14.1.1

kubectl port-forward -n prometheus svc/prometheus 9090:9090