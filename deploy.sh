#!/usr/bin/env bash
ssh -o StrictHostKeyChecking=no root@155.200.30.30 << 'ENDSSH'
 cd /django
 docker login -u $REGISTRY_USER -p $CI_BUILD_TOKEN $CI_REGISTRY
 docker pull registry.gitlab.com/jaysalpatel/django-resume-blog:latest
 docker-compose up -d
ENDSSH


### ssh automatically maintains and checks a database 
##containing identification for all hosts it has ever been used 
##with. Host keys are stored in ~/. ... The ssh_config keyword 
##StrictHostKeyChecking can be used to control logins to machines
## whose host key is not known or has changed.